const { PersonaModel } = require('../models');

function respuesta (finalizado, mensaje, datos) {
    return {
        finalizado,
        mensaje,
        datos
    }
}

const listarPersonas = async (req, res) => {
    try {
        const listaPersonas = await PersonaModel.find();
        res.status(200).json(respuesta(true, 'Listado de Personas', listaPersonas));
    } catch (error) {
        res.status(400).json(respuesta(false, 'Error en el listado de personas.', error.message));
    }
};

const registrarPersona = async (req, res) => {
    try {
        const datos = req.body;
        const personaCreada = await PersonaModel.create(datos);
        res.status(201).json(respuesta(true, 'Persona Resgistrada Correctamente.', personaCreada));
    } catch (error) {
        res.status(400).json(respuesta(false, 'Persona NO Registrada.', error.message));
    }
};

const eliminarPersona = async (req, res) => {
    const { idPersona }= req.params;
    try {
        const personaEliminada = await PersonaModel.findByIdAndRemove(idPersona);
        res.status(200).json(respuesta(true, 'Persona Eliminada Correctamente', personaEliminada));
    } catch (error) {
        res.status(400).json(respuesta(false, `Persona NO Eliminada: ${idPersona}`, error.message)); 
    }
};

const modificarPersona = async (req, res) => {
    const { idPersona } = req.params;
    try {
        const datos = req.body;
        const personaModificada = await PersonaModel.findByIdAndUpdate(idPersona, datos);
        console.log('================================');
        console.log(personaModificada);
        console.log('================================');
        res.status(201).json(respuesta(true, 'Persona Modificada Correctamente.', personaModificada));
    } catch (error) {
        res.status(400).json(respuesta(false, `Persona NO Modificada: ${idPersona}`, error.message));
    }
};

module.exports = {
    listarPersonas,
    registrarPersona,
    eliminarPersona,
    modificarPersona
};