const express = require('express');
const router = express.Router();
const { PersonaController } = require('../controllers');

// Listar Peronas
router.get('/personas', PersonaController.listarPersonas);
// Registrar Persona
router.post('/personas', PersonaController.registrarPersona);
// Eliminar Persona
router.delete('/personas/:idPersona', PersonaController.eliminarPersona);
// Modificar Persona
router.put('/personas/:idPersona', PersonaController.modificarPersona);

module.exports = router;